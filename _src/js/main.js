// OBS: ESTÁ SENDO UTILIZADO O JQUERY DO PRÓPRIO WP.
jQuery(function($) {
  $(document).ready(function() {
    
    new WOW().init();

    function searchDesktop() {

      var event_busca = $('#busca-topo');
      var event_close = $('#close');
      var box_form    = $('#pa-component-busca');
      var el_form     = $('#search-filter-form-122');
      var el_input    = $('#search-filter-form-122').find('input[type = text]');
      var btnSubmit   = $('#search-filter-form-122').find('input[type = submit]');

      el_input.attr('required','required');
      
      event_busca.click(function (event) {
        event.preventDefault();
        box_form.fadeIn(200);
        event_close.addClass('fa-times').fadeIn(150);
        el_input.val("");
        btnSubmit.val("Buscar");
      });
      
      event_close.click(function () {
        box_form.fadeOut(200);
      });

      el_form.submit(function() {
        
        el_input.val("").attr('placeholder','Pesquisando...');
        btnSubmit.val("Buscar");
        
      });

    }
    searchDesktop();

    function menuScroll() {

      $('a[href*="#"]:not([href="#"])').click(function() {
        
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            
            return false;
          }
        }
        
      });
      
    }
    menuScroll();


    function textAnimation() {

      var TxtRotate = function(el, toRotate, period) {
        this.toRotate   = toRotate;
        this.el         = el;
        this.loopNum    = 0;
        this.period     = parseInt(period, 10) || 2000;
        this.txt        = '';
        this.tick();
        this.isDeleting = false;
      };
      
      TxtRotate.prototype.tick = function() {

        var i       = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];
      
        if (this.isDeleting) {

          this.txt = fullTxt.substring(0, this.txt.length - 1);

        } else {

          this.txt = fullTxt.substring(0, this.txt.length + 1);

        }
      
        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';
      
        var that  = this;
        var delta = 300 - Math.random() * 100;
      
        if (this.isDeleting) { 
          delta /= 2; 
        }
      
        if (!this.isDeleting && this.txt === fullTxt) {

          delta = this.period;
          this.isDeleting = true;
        
        } else if (this.isDeleting && this.txt === '') {

          this.isDeleting = false;
          this.loopNum++;
          delta = 500;

        }
      
        setTimeout(function() {
          that.tick();
        }, delta);

      };
      
      
      var elements = document.getElementsByClassName('txt-rotate');
      
      for (var i=0; i<elements.length; i++) {
        
        var toRotate = elements[i].getAttribute('data-rotate');
        var period   = elements[i].getAttribute('data-period');
        
        if (toRotate) {
          new TxtRotate(elements[i], JSON.parse(toRotate), period);
        }
    
      }

      // INJECT CSS
      var css = document.createElement("style");
      
      css.type = "text/css";
      css.innerHTML = ".txt-rotate > .wrap { animation: pulse .4s ease-in-out infinite; } @keyframes pulse { 0% { border-right: 5px solid transparent; } 50% { border-right: 5px solid #292929; } 100% { border-right: 5px solid transparent; } }";
      document.body.appendChild(css); 

      $(function() {
          
        $('a[href="#!..."]').on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
        });

      });

    }
    textAnimation();


    function popupPrograma() {

      $('#pa-component-modal').magnificPopup({
        removalDelay: 500,
        callbacks: {

          beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
  
            var video = this.st.el.attr('data-video');
            var html  = '<div class="pa-component-iframe"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + video + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe></div>';
  
            $(this.st.el.attr('href')).html(html).fadeIn(1000);
          },

          close: function () {
            $(this.st.el.attr('href')).html('');
          }
        },
        midClick: true
      });

    }
    popupPrograma();


    function owlMentores() {
      $("#pa-component-owl").owlCarousel({
        items: 1,
        mouseDrag: false,
        animateOut: 'fadeOut',
        loop: true,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        responsive: {
          0: {
            nav: true,
            dots: false,
            items: 1
          },
          600: {
            dots: false,
            nav: true,
            items: 1,
          },
          1000: {
            dots: false,
            nav: true,
            items: 1,
          }
        }
      });
    }
    owlMentores();

    function animateJornada() {

      var $item = $('.pa-component-wrap-jornada__item');

      $item.hide().each(function(index) {
        $(this).delay(700 * index).fadeIn(700);
      });
    }
    animateJornada();

    function animateBeneficios() {

      var $item = $('.pa-component-wrap-beneficios__item');

      $item.hide().each(function(index) {
        $(this).delay(700 * index).fadeIn(700);
      });

    }
    animateBeneficios();

    function animateFeedbacks() {

      var $item = $('.pa-component-wrap-feedback-group__item');

      $item.hide().each(function(index) {
        $(this).delay(700 * index).fadeIn(700);
      });

    }
    animateFeedbacks();

    function animatePostagem() {

      var $item = $('.pa-component-article');

      $item.hide().each(function(index) {
        $(this).delay(700 * index).fadeIn(700);
      });

    }
    animatePostagem();


    function animateTagPost() {

      var $item = $('.pa-single-content__article');

      $item.hide().each(function(index) {
        $(this).delay(700 * index).fadeIn(700);
      });

    }
    animateTagPost();
   

    function maskPhone() {
      $("#fone").mask("(99)9999-9999?9");
    }
    maskPhone();


 

    function isBlog(){
      var elem = $('.blog').find('#menu-header').children();
      var elem2 = $('.single').find('#menu-header').children();
      var menu = $('.blog').find('#menu-header');
      var menu2 = $('.single').find('#menu-header');
      
      menu.css('justify-content','flex-end');
      menu2.css('justify-content','flex-end');

      for(var i = 0; i < 5; i++){
        elem.eq(i).css('display', 'none');
        elem2.eq(i).css('display', 'none');
      }
    }
    isBlog();

    function scrollTop() {
    
      $(window).scroll(function(){
        if($(this).scrollTop() > 100){
          $('#scroll').fadeIn();
        }else{
          $('#scroll').fadeOut();
        }
      });
  
      $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
      });
      
    }
    scrollTop();







  });
});

