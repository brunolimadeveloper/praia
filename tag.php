<?php get_template_part('templates/html','header');?>
<div class="pa-single">
    <div class="container">
        <h2 class="pa-single__title">Tag: <?php echo single_tag_title(); ?></h2>
        <div class="pa-single-content">
            <div class="pa-single-content__post">
                <?php 
                    global $exclud_id_postagem;
                    global $wp_query;
                    query_posts(array_merge($wp_query->query));
                    while (have_posts()) : the_post(); 
                    $exclud_id_postagem[] = get_the_ID();
                ?>

                <?php include(locate_template('templates/content/content.php')); ?>
                
                <?php endwhile; wp_pagenav($wp_query->max_num_pages); wp_reset_postdata(); ?>
            </div>
            <?php include(locate_template('sidebar.php')); ?>
        </div>
    </div>
</div>
<?php get_template_part('templates/html','footer');?>

