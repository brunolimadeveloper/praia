<?php $settings = get_option('options_gerais'); 
    foreach ($settings['image'] as $img) :
        $thumb = wp_get_attachment_image_url( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
    endforeach;
    foreach ($settings['image-effect1'] as $img) :
        $imgEffect1 = wp_get_attachment_image_url( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
    endforeach;
    foreach ($settings['image-effect2'] as $img) :
        $imgEffect2 = wp_get_attachment_image_url( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
    endforeach;
?>
<section class="pa-section-inscricoes" id="inscricoes">
    <div class="container">
        <div class="pa-component-wrap">
            <div class="pa-component-wrap__figure">
                <!-- fuguete -->
                <div class="image-effect1 wow fadeInUp" data-wow-duration="3s" data-wow-delay="1s">
                    <img src="<?php echo $imgEffect1; ?>" alt="Inscrições">
                </div>
                <figure class="image wow fadeInLeft" data-wow-duration="4s" data-wow-delay="1s">
                    <img src="<?php echo $thumb; ?>" alt="Inscrições">
                </figure>
                <!-- nuvem -->
                <div class="image-effect2 wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
                    <img src="<?php echo $imgEffect2; ?>" alt="Inscrições">
                </div>
            </div>
            <div class="pa-component-wrap__text">
                <?php $desc = $settings['desc']; ?>
                <h2 class="title"><?php echo $settings['title']; ?></h2>
                <p class="desc txt-rotate" data-period="1000" data-rotate='[ "<?php echo $desc;?>", "", ""  ]'></p>
                <div class="ctas">
                    <?php if($settings['url']): ?>
                        <a href="<?php echo $settings['url']; ?>" target="_blank" title="<?php echo $settings['btn'] ? $settings['btn'] : 'Praia'; ?>" class="pa-btn pa-btn--inscrever"><?php echo $settings['btn'] ? $settings['btn'] : 'Insira um texto'; ?></a>
                        <?php endif; ?>
                    <?php if($settings['baixar']): ?>
                        <a href="<?php echo $settings['baixar']; ?>" download target="_blank" title="<?php echo $settings['btnDownload'] ? $settings['btnDownload'] : 'Praia'; ?>" class="pa-btn pa-btn--inscrever"><?php echo $settings['btnDownload'] ? $settings['btnDownload'] : 'Insira um texto'; ?></a>
                    <?php endif; ?>
                    <?php if($settings['baixar_doc_insc']): ?>
                        <a href="<?php echo $settings['baixar_doc_insc']; ?>" download target="_blank" title="<?php echo $settings['btnDownloadInsc'] ? $settings['btnDownloadInsc'] : 'Praia'; ?>" class="pa-btn pa-btn--inscrever"><?php echo $settings['btnDownloadInsc'] ? $settings['btnDownloadInsc'] : 'Insira um texto'; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="pa-component-wrap__scroll">
            <a href="#" class="scroll-icon"></a> 
        </div>
    </div>
</section>