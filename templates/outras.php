<section class="pa-section-outras">
    <div class="container">
        <header class="pa-section-outras__header">
            <h2 class="pa-section-outras__title">Pegue outras ondas</h2>
            <hr class="line">
        </header>
        <div class="pa-component-wrap-postagem">
            <?php $recents = new WP_Query( array('post_type'=>'post','posts_per_page'=>3,"orderby"=>"date",'post__not_in' =>$exclud_id_postagem,"order"=> "DESC" )); 
                if ( $recents->have_posts() ) : 
                    while ( $recents->have_posts() ) : $recents->the_post(); 
            ?>
                
            <?php include(locate_template('templates/content/loop-post.php')); ?>

            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
        </div>
    </div>
</section>