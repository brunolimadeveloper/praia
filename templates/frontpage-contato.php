<section class="pa-section-contato">
    <div class="container">
        <div class="pa-component-wrap-footer-form">
            <header class="pa-section-footer-head">
                <h2 class="pa-section-footer-head__title">Contato</h2>
                <hr class="line">
            </header>
            <div class="pa-component-form-contato">
                <?php echo do_shortcode('[contact-form-7 id="19" title="Formulário de contato"]'); ?>
            </div>
        </div>
    </div>
</section>