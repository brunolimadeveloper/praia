<?php get_template_part('templates/html', 'head'); ?>
<header class="pa-component-header">
    <div class="pa-component-header__bar">
	    <div class="container">
            <div class="pa-component-header__area-associado">
                <a href="#" target="_blank" title="Área do associado" class="link-associado"><i class="user fa fa-user-circle-o" aria-hidden="true"></i><span class="txt">Área do associado<span></a>
            </div>
        </div>
    </div>
    <div class="pa-component-header__navegation">
        <div class="container">
            <div class="pa-component-header__wrap">
                <a href="<?php echo get_site_url(); ?>" title="Praia" class="pa-component-header__link">
                    <h1 class="pa-component-header__praia">
                        <img src="<?php echo get_theme_mod( 'logo_header_desktop' ); ?>" alt="Praia" class="thumb">
                    </h1>
                </a>
                <nav class="pa-component-header__nav">
                    <?php wp_nav_menu(array('theme_location' => 'menu_1', 'menu_class' => 'pa-component-menu')); ?>
                    <a href="#" class="pa-component-header__busca fa  fa-search" id="busca-topo"></a>
                </nav>
            </div>
        </div>
    </div>
</header>
<div class="pa-component-busca" id="pa-component-busca">
    <div class="container">
        <div class="busca">
            <?php echo do_shortcode('[searchandfilter id="119"]'); ?>
            <a href="#" id="close" class="close fa "></a>
        </div>
    </div>
</div>
<main class="main" role="main">