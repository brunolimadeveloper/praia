<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-programa" id="programa">
    <div class="container">
        <div class="pa-component-wrap-sobre">
            <div class="pa-component-wrap-sobre__txt">
                <h2 class="title"><?php echo $settings['sobre_title'];?></h2>
                <hr class="line">
                <div class="desc"><?php echo $settings['sobre_desc'];?></div>
            </div>
            <div class="pa-component-wrap-sobre__figure">
                <div class="pa-component-wrap-sobre__video">
                    <?php foreach ($settings['sobre_image'] as $img) :
                        echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => 'O programa', 'title' => 'O programa' ));
                    endforeach; ?>
                    <a href="#video_popup" class="pa-component-wrap-sobre__play" data-video="<?php echo $settings['sobre_video']; ?>" id="pa-component-modal"></a>
                    <div id="video_popup" class="mfp-modal mfp-hide"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="pa-component-wrap-newsletter">
            <header class="pa-component-wrap-newsletter__header">
                <h2 class="title">Fique por dentro antes de todo mundo</h2>
                <p class="desc">Olá, Praieiro! Quer ficar por dentro de todas as novidades do Praia?<br/> Então assine a brisa, a newsletter onde os bons ventos sempre sopram.</p>
            </header>
            <div class="pa-component-wrap-newsletter__form">
                <?php echo do_shortcode('[contact-form-7 id="94" title="Formulário Newsletter"]'); ?>
            </div>
        </div>  
    </div>
</section>
<section class="pa-section-jornada">
    <div class="container">
        <div class="pa-component-wrap-jornada">
            <div class="pa-component-wrap-jornada__item">
            <header class="pa-section-jornada-head">
                <h2 class="pa-section-jornada-head__title"><?php echo $settings['title-jornada'];?></h2>
                <div class="pa-section-jornada-head__desc"><?php echo $settings['desc-jornada'];?></div>
            </header>
            </div>
            <?php foreach ($settings['jornada'] as $item): ?>
                <div class="pa-component-wrap-jornada__item">
                    <figure class="pa-component-wrap-jornada__figure">
                        <?php foreach ($item['jornada_image'] as $img) :
                            echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title()));
                        endforeach; ?>
                    </figure>
                    <div class="pa-component-wrap-jornada__info">
                        <div class="title"><?php echo $item['jornada_tit']; ?></div>
                        <div class="desc"><?php echo $item['jornada_desc']; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="pa-section-jornada__btn">
            <?php if($settings['url']): ?>
                <a href="<?php echo $settings['url']; ?>" target="_blank" title="<?php echo $settings['btn'] ? $settings['btn'] : 'Praia'; ?>" class="pa-btn pa-btn--inscrever pa-btn--medium"><?php echo $settings['btn'] ? $settings['btn'] : 'Insira um texto'; ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>