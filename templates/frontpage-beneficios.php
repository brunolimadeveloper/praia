<?php $settings = get_option('options_gerais'); 
    foreach ($settings['beneficios_img'] as $img) :
        $imgBefore = wp_get_attachment_image_url( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
    endforeach;
?>
<section class="pa-section-beneficios" id="beneficios">
    <div class="container">
        <header class="pa-section-beneficios-head">
            <img src="<?php echo $imgBefore; ?>" alt="Benefícios" class="pa-section-beneficios-head__img">
            <h2 class="pa-section-beneficios-head__title"><?php echo $settings['beneficios_title'];?></h2>
            <hr class="line">
        </header>
        <div class="pa-component-wrap-beneficios">
            <?php foreach ($settings['beneficios_options'] as $item): ?>
                <div class="pa-component-wrap-beneficios__item">
                    <figure class="figure">
                        <?php foreach ($item['beneficios_options_img'] as $img) :
                            echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
                        endforeach; ?>
                    </figure>
                    <div class="legend">
                        <div class="tit"><?php echo $item['beneficios_options_title']; ?></div>
                        <div class="desc"><?php echo $item['beneficios_options_desc']; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="pa-beneficio-cta">
            <?php if($settings['baixar_cesta']): ?>
                <a href="<?php echo $settings['baixar_cesta']; ?>" download target="_blank" title="<?php echo $settings['btnDownloadCesta'] ? $settings['btnDownloadCesta'] : 'Praia'; ?>" class="pa-btn pa-btn--large pa-btn--inscrever"><?php echo $settings['btnDownloadCesta'] ? $settings['btnDownloadCesta'] : 'Insira um texto'; ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>