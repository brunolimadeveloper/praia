<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-teses" id="teses">
    <div class="container">
        <div class="pa-component-wrap-teses">
            <figure class="pa-component-wrap-teses__figure wow fadeInLeft" data-wow-duration="3s" data-wow-delay="1s">
                <?php foreach ($settings['tese_image'] as $img) :
                    echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => 'Teses', 'title' => 'Teses' ));
                endforeach; ?>
            </figure>
            <div class="pa-component-wrap-teses__text">
                <h2 class="title"><?php echo $settings['tese_title'];?></h2>
                <hr class="line">
                <div class="desc"><?php echo $settings['tese_desc'];?></div>
                <div class="bloco">
                    <div class="bloco-interesse">
                        <h3 class="tit"><?php echo $settings['area_text'];?></h3>
                        <div class="interesses">
                            <?php foreach ($settings['teses_interesse'] as $item): ?>
                                <h5 class="item"><?php echo $item['interesse_item']; ?></h5>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <div class="bloco-interesse bloco-interesse--tec">
                        <h3 class="tit"><?php echo $settings['area_tec_text'];?></h3>
                        <div class="interesses">
                            <?php foreach ($settings['teses_tecnologia'] as $item): ?>
                                <h5 class="item"><?php echo $item['teses_tecnologia_item']; ?></h5>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>