<section class="pa-section-news">
    <div class="container">
        <header class="pa-section-blog-head">
            <h2 class="pa-section-blog-head__title">Blog</h2>
            <hr class="line">
        </header>   
        <div class="pa-component-wrap-news">
        <?php 
            $i=1;
            $the_query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 2, "orderby" => "date","order" => "DESC" )); 
            if ( $the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : $the_query->the_post(); 
               
        ?>
            <article class="pa-component-news-article <?php echo ( $i % 2 == 0 ) ? 'pa-component-news-article--reverse' : '';?>" id="post-ID-<?php the_ID(); ?>">
                <figure class="pa-component-news-article__figure">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('thumb-medium', array('class' => 'fade thumb', 'alt' => get_the_title(), 'title' => get_the_title())); ?>
                    </a>
                </figure>
                <div class="pa-component-news-article__legend">
                    <a href="<?php the_permalink(); ?>"><h2 class="pa-component-news-article__title" title="<?php echo get_the_title(); ?>"><?php the_title(); ?></h2></a>
                    <div class="pa-component-news-article__desc"><?php echo get_the_excerpt(); ?></div>
                    <a href="<?php the_permalink(); ?>" class="pa-component-news-article__link" title="Ver mais">Leia mais <i class="fa fa-angle-right"></i></a>
                </div>
            </article>
        <?php $i++; endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
        </div>
        <div class="pa-section-news__btn">
            <a href="<?php echo get_site_url(); ?>/blog" title="<?php the_title_attribute('before=Ver mais postagem em: '); ?>" class="pa-btn pa-btn--inscrever pa-btn--medium">Leia mais</a>
        </div>
    </div>
</section>
