</main>
<?php $settings = get_option('options_gerais'); ?>
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>
<footer class="pa-footer" id="contato">
    <div class="container">
        <div class="pa-component-wrap-footer">
            
            <div class="pa-component-wrap-footer-marcas">

                <div class="pa-component-mantenedores">
                    <h3 class="pa-component-wrap-footer-marcas__title">Mantenedore</h3>
                    <div class="pa-component-wrap-footer-mantenedores">
                        <?php foreach ($settings['footer_mantenedores'] as $item): ?>
                            <div class="pa-component-wrap-footer-mantenedores__item">
                                <figure class="figure">
                                    <?php foreach ($item['footer_mantenedores_img'] as $img) :
                                        echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
                                    endforeach; ?>
                                </figure>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                
                <div class="pa-component-parceiros">
                    <h3 class="pa-component-wrap-footer-marcas__title">Parceiros</h3>
                    <div class="pa-component-wrap-footer-parceiros">
                        <?php foreach ($settings['footer_parceiros'] as $item): ?>
                            <div class="pa-component-wrap-footer-parceiros__item">
                                <figure class="figure">
                                    <?php foreach ($item['footer_parceiros_img'] as $img) :
                                        echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
                                    endforeach; ?>
                                </figure>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                

            </div>
        </div>
    </div>
    <div class="pa-bar-footer">
        <div class="pa-component-wrap-footer-bar">
            <div class="container">
                <div class="pa-component-wrap-footer-bar__box">
                    <div class="pa-component-wrap-footer-bar__logo">
                        <a href="<?php echo get_site_url(); ?>" title="Praia">
                            <img src="<?php echo get_theme_mod( 'logo_footer_desktop' ); ?>" alt="Praia" class="thumb">               
                        </a>
                    </div>
                    <div class="pa-component-wrap-footer-bar__bar"></div>
                    <div class="pa-component-wrap-footer-bar__network">
                        <a href="<?php echo $settings['footer_youtube'];?>" title="Youtube" class="link"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        <a href="<?php echo $settings['footer_instagram'];?>" title="Instagram" class="link"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="mailto:<?php echo $settings['footer_mail'];?>" title="Email" class="link"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <a href="<?php echo $settings['footer_linkedin'];?>" title="Linkedin" class="link"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pa-component-wrap-footer-bar__cvtt">
            <div class="container">
            <a href="https://www.convertte.com.br" target="_blank" title="https://www.convertte.com.br">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cvtt.png" alt="Convertte" class="thumb">               
                </a>
            </div>
        </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>