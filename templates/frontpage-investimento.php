<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-investimento">
    <div class="container">
        <header class="pa-header-investimento">
            <h2 class="pa-header-investimento__title"><?php echo $settings['inv_title']; ?></h2>
            <hr class="line">
        </header>
        <div class="pa-header-investimento__text"><?php echo $settings['inv_text']; ?></div>
    </div>
</section>