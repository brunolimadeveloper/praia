<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-cronograma" id="cronograma">
    <div class="container">
        <header class="pa-section-cronograma-head">
            <h2 class="pa-section-cronograma-head__title"><?php echo $settings['cronograma_title'];?></h2>
            <hr class="line">
        </header>
        <div class="pa-component-wrap-cronograma">
            <?php $i=1; foreach ($settings['cronograma_options'] as $item): ?>
                <div class="pa-component-wrap-cronograma__item <?php echo ( $i % 2 == 0 ) ? 'pa-component-wrap-cronograma__item--reverse' : '';?>">
                    <div class="pa-component-wrap-cronograma__info">
                        <div class="data wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="1s"><time><?php echo $item['cronograma_options_data']; ?></time></div>
                        <div class="title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1s"><?php echo $item['cronograma_options_title']; ?></div>
                        <div class="desc wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.5s"><?php echo $item['cronograma_options_desc']; ?></div>
                    </div>
                    <figure class="pa-component-wrap-cronograma__figure wow <?php echo $item['jornada_options']; ?>">
                        <?php foreach ($item['cronograma_options_img'] as $img) :
                            echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb wow fadeIn", 'alt' => get_the_title(), 'title' => get_the_title(), 'data-wow-duration' => '1.5s', 'data-wow-delay' => '1s' ));
                        endforeach; ?>
                    </figure>
                </div>
            <?php $i++; endforeach; ?>
        </div>                    
    </div>
</section>