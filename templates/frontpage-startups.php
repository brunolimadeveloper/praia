<?php $settings = get_option('options_gerais'); ?>

<section class="pa-section-startups">
    <div class="container">
        <h2 class="pa-section-startups__h3"><?php echo $settings['section-startups-title'] ? $settings['section-startups-title'] : ''; ?></h2>
        <hr class="pa-section-startups__linha">

        <div class="pa-section-startups-cards">
        <?php if($settings['section-startups-cards']): foreach($settings['section-startups-cards'] as $cards): ?>
            <div class="pa-section-startups-card">
            <?php foreach($cards['section-startups-img'] as $card): ?>
                <?php $url = wp_get_attachment_image_url($card,'','',array('class'=>'','alt'=>get_the_title())); ?>
                <a href="<?php echo $cards['section-startups-link']; ?> " target="_blank" ><img src="<?php echo $url; ?>" class="pa-section-startups-thumb"></a>
            <?php endforeach; ?>
             </div>
        <?php endforeach; endif;?>
        </div>
    </div>
</section>