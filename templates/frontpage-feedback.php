<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-feedback-img"></section>
<section class="pa-section-feedback" id="feedback">
    <div class="container">
        <div class="pa-component-wrap-feedback">
            <header class="pa-section-feedback-head">
                <h2 class="pa-section-feedback-head__title wow fadeInLeft" data-wow-duration="2.5s" data-wow-delay="1s"><?php echo $settings['feedback_title']; ?></h2>
                <div class="pa-section-feedback-head__desc  wow fadeInLeft" data-wow-duration="2.2s" data-wow-delay="1s"><?php echo $settings['feedback_desc']; ?></div>
                <?php if($settings['url']): ?>
                    <a href="<?php echo $settings['url']; ?>" target="_blank" title="<?php echo $settings['btn'] ? $settings['btn'] : 'Praia'; ?>" class="pa-btn pa-btn--inscrever pa-btn--medium pa-btn--white wow fadeInLeft" data-wow-duration="2.4s" data-wow-delay="1s"><?php echo $settings['btn'] ? $settings['btn'] : 'Insira um texto'; ?><i class="fa fa-angle-right"></i></a>
                <?php endif; ?>
            </header>
            <div class="pa-component-wrap-feedback-group">
                <?php foreach ($settings['feedback_options'] as $item): ?>
                    <div class="pa-component-wrap-feedback-group__item">
                        <figure class="figure">
                            <?php foreach ($item['feedback_options_img'] as $img) :
                                echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
                            endforeach; ?>
                        </figure>
                        <div class="legend">
                            <div class="desc"><?php echo $item['feedback_options_desc']; ?></div>
                            <div class="tit" style="color: <?php echo $item['feedback_cor']; ?>;"><?php echo $item['feedback_options_title']; ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>