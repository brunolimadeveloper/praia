<?php $settings = get_option('options_gerais'); ?>
<section class="pa-section-mentores" id="mentores">
    <div class="container">
        <div class="pa-component-wrap-mentores">
            <header class="pa-section-mentores-head">
                <h2 class="pa-section-mentores-head__title"><?php echo $settings['mentores_title'];?></h2>
                <hr class="line">
            </header>
            <div class="pa-component-wrap-mentores-owl" id="pa-component-owl">
                <?php foreach ($settings['mentores_carrousel'] as $item): ?>
                    <div class="pa-component-wrap-mentores-owl__item wow fadeIn" data-wow-duration="1.5s" data-wow-delay="1s">
                        <figure class="figure">
                            <?php foreach ($item['mentores_owl_img'] as $img) :
                                echo wp_get_attachment_image( $img, 'full', "", array("class" => "thumb", 'alt' => get_the_title(), 'title' => get_the_title() ));
                            endforeach; ?>
                        </figure>
                        <div class="legend">
                            <div class="tit"><?php echo $item['mentores_owl_title']; ?></div>
                            <div class="desc"><?php echo $item['mentores_owl_desc']; ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>