<?php 
    $cat   = get_the_category(); 
    $color = get_term_meta($cat[0]->term_id,'color',true);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('pa-single-content__article'); ?>>
    <figure class="pa-single-content__figure">
        <?php the_post_thumbnail('thumb-medium', array('class' => 'fade thumb', 'alt' => get_the_title(), 'title' => get_the_title())); ?>
    </figure>
    <header class="pa-single-content__header">
        <h2 class="pa-single-content__title"><?php the_title(); ?></h2>
        <div class="pa-single-content__head">
            <a href="<?php echo get_category_link($cat[0]->term_id); ?>" class="pa-single-content__categoria" style="color: <?php echo $color ? $color : '#4c13fb'; ?>;"><i class="pa-icon pa-icon--folder"></i><span><?php echo $cat[0]->name; ?></span></a>
            <div class="pa-single-content__date"><i class="pa-icon pa-icon--date"></i><span><?php echo get_the_date(); ?></span></div>
        </div>
    </header>
    <?php $tags = wp_get_post_terms(get_the_ID(), 'post_tag');
        if( $tags ): ?>
            <div class="pa-single-content__tags">
                <?php foreach ( $tags as $tag ) :
                    $tag_link = get_tag_link( $tag->term_id ); ?> 
                    <a href="<?php echo $tag_link; ?>" title="<?php echo $tag->name;?>" class="tag"><?php echo $tag->name;?></a>
                <?php endforeach; ?>
            </div>
    <?php endif; ?>
	<div class="pa-single-content__text">
		<?php
            the_content(
                sprintf(
                    /* translators: %s: Post title. */
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', '' ),
                    get_the_title()
                )
            );
		?>
    </div>
</article>
