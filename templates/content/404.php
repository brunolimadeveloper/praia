<div class="pa-component-info-404">
    <h2 class="pa-component-info-404__title"><span>Oops! </span>404</h2>
    <h3 class="pa-component-info-404__subtitle">Essa página não existe ou não foi encontrada.</h3>
    <p class="pa-component-info-404__desc">A página que você estava procurando não foi encontrada. <br>A página também pode ter tido seu endereço alterado ou está temporariamente indisponível.</p>
    <a class="pa-btn pa-btn--large pa-btn--purple" href="<?php echo get_site_url(); ?>">Acesse a página inicial!</a>
</div>