<?php 
    $cat   = get_the_category(); 
    $color = get_term_meta($cat[0]->term_id,'color',true);
?>
<article class="pa-component-article <?php echo $count; ?>" id="post-ID-<?php the_ID(); ?>">
    <figure class="pa-component-article__figure">
        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
            <?php the_post_thumbnail('thumb-medium', array('class' => 'fade thumb', 'alt' => get_the_title(), 'title' => get_the_title())); ?>
        </a>
    </figure>
    <div class="pa-component-article__legend">
        <?php if(is_category() || is_archive()): ?>
            <div class="pa-component-article__categoria" style="color: <?php echo $color ? $color : '#4c13fb'; ?>;"><i class="pa-icon pa-icon--folder"></i><span><?php echo single_cat_title(); ?></span></div>
        <?php else: ?>
            <a href="<?php echo get_category_link($cat[0]->term_id); ?>" class="pa-component-article__categoria" style="color: <?php echo $color ? $color : '#4c13fb'; ?>;"><i class="pa-icon pa-icon--folder"></i><span><?php echo $cat[0]->name; ?></span></a>
        <?php endif; ?>
        <a href="<?php the_permalink(); ?>"><h2 class="pa-component-article__title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
        <a href="<?php the_permalink(); ?>" class="pa-component-article__link" title="<?php the_title_attribute('before=Ver mais sobre '); ?>">Ver mais <i class="fa fa-angle-right"></i></a>
    </div>
</article>