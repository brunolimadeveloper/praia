<?php get_template_part('templates/html','header');?>

<div class="pa-page">
    <section class="pa-section-postagem">
        <div class="container">
            <h2 class="pa-section-postagem__title"><?php echo single_cat_title(); ?></h2>
            <div class="pa-component-wrap-postagem">
                <?php 
                    if ( have_posts() ) :
                    while ( have_posts() ) : the_post(); 
                ?>
                   
                <?php include(locate_template('templates/content/loop-post.php')); ?>

               <?php endwhile; else : ?>
                    <p><?php __('No News'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('templates/html','footer');?>

