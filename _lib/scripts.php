<?php
/*
* Scripts
* Desenvolvedor: Bruno Kiedis
*/

function call_script() {
    wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.min.css', array(), '1.0', null);
    //wp_register_script('maps-google', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyADZ5cJNKoRXvQ1K1nUkx61uvWcGMe85IE', false, null, true); 
    wp_register_script('main', get_template_directory_uri() . '/assets/js/main.min.js', array(), '1.0', true);
    
    //wp_enqueue_script('maps-google');
    wp_enqueue_script('main');
}

add_action('wp_enqueue_scripts', 'call_script', 100);


function admin_script() {
    wp_enqueue_style('inputs', get_template_directory_uri() . '/_lib/_admin/assets/css/input-styles.css', array(), null);
}

add_action('admin_enqueue_scripts', 'admin_script', 100);

function load_custom_admin_styles() {
    wp_register_style( 'admin-styles', get_stylesheet_directory_uri() . '/_lib/_admin/assets/css/print.css', false, '1.0.0' );
    wp_enqueue_style( 'admin-styles' );
}
 
add_action( 'admin_enqueue_scripts', 'load_custom_admin_styles' );