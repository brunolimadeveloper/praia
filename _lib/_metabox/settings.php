<?php
/*
* Metabox Settings
* Desenvolvedor: Bruno Kiedis
*/

// CREATE PAGE
add_filter( 'mb_settings_pages', 'prefix_options_page' );
function prefix_options_page($settings_pages){

  $settings_pages[] = array(
    'id'          => 'theme-options',
    'option_name' => 'options_gerais',
    'menu_title'  => 'Frontpage',
  );
  return $settings_pages;

}

// START DEFINITION OF META BOXES
add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes ) {

  // GTM
  $meta_boxes[] = array(
    'id'        => 'gtm_codes',
    'title'     => 'GTM Code',
    'context'   => 'side',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(

      array(
        'name'  => '',
        'id'    => 'gtm_head',
        'desc'  => 'Code Head',
        'type'  => 'textarea',
      ),

      array(
        'name'  => '',
        'id'    => 'gtm_body',
        'desc'  => 'Code Body',
        'type'  => 'textarea',
      ),
    )
  );

  // INSCRIÇÕES
  $meta_boxes[] = array(
    'id'        => 'inscricoes',
    'title'     => 'Inscrições',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      array(
        'id'               => 'image',
        'name'             => 'Imagem',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 
      array(
        'id'               => 'image-effect1',
        'name'             => 'Imagem efeito 1',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 
      array(
        'id'               => 'image-effect2',
        'name'             => 'Imagem efeito 2',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 
      array(
        'id'   => 'title',
        'name' => 'Título',
        'type' => 'text',
      ),
      array(
        'id'   => 'desc',
        'name' => 'Descrição',
        'type' => 'text',
      ),
      array(
        'id'   => 'url',
        'name' => 'URL do botão',
        'type' => 'text',
      ),
      array(
        'id'   => 'btn',
        'name' => 'Texto do botão',
        'type' => 'text',
      ),
      array(
        'id'   => 'baixar',
        'name' => 'URL do arquivo',
        'type' => 'text',
      ),
      array(
        'id'   => 'btnDownload',
        'name' => 'Texto do botão download',
        'type' => 'text',
      ),
      array(
        'id'   => 'baixar_cesta',
        'name' => 'URL do arquivo',
        'type' => 'text',
      ),
      array(
        'id'   => 'btnDownloadCesta',
        'name' => 'Texto do botão download',
        'type' => 'text',
      ),
      array(
        'id'   => 'baixar_doc_insc',
        'name' => 'URL do arquivo',
        'type' => 'text',
      ),
      array(
        'id'   => 'btnDownloadInsc',
        'name' => 'Texto do botão download',
        'type' => 'text',
      ),
    )
  );
  
  // PROGRAMA
  $meta_boxes[] = array(
    'id'        => 'video',
    'title'     => 'Sobre o Programa',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'               => 'sobre_image',
        'name'             => 'Imagem para o vídeo',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 

      array(
        'id'   => 'sobre_title',
        'name' => 'Título',
        'type' => 'text',
      ), 

      array(
        'id'   => 'sobre_desc',
        'name' => 'Descrição sobre o projeto',
        'type' => 'textarea',
      ), 

      array(
        'id'   => 'sobre_video',
        'name' => 'ID do vídeo de apresentação',
        'desc' => 'Insira o ID do vídeo - Youtube',
        'type' => 'text',
      ), 
    )
  );
  
  // JORNADA
  $meta_boxes[] = array(
    'id'        => 'sobre_jornada',
    'title'     => 'Jornada',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'   => 'title-jornada',
        'name' => 'Título',
        'type' => 'text',
      ), 

      array(
        'id'   => 'desc-jornada',
        'name' => 'Descrição',
        'type' => 'textarea',
      ), 

      array(
        'id'          => 'jornada',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '6',
        'group_title' => 'Detalhes',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'jornada_image',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ),

          array(
            'id'   => 'jornada_tit',
            'name' => 'Título',
            'type' => 'text',
          ),  
          
          array(
            'id'   => 'jornada_desc',
            'name' => 'Descrição',
            'type' => 'textarea',
          ),  

          // array(
          //   'id'              => 'jornada_options',
          //   'name'            => 'Animação',
          //   'type'            => 'select',
          //   'options'         => array(  
          //     'bouceIn'             => 'bouceIn',
          //     'bounce'              => 'bounce',
          //     'fadeIn'              => 'fadeIn',
          //     'fadeInUp'            => 'fadeInUp',
          //     'fadeInLeft'          => 'fadeInLeft',
          //     'fadeInRight'         => 'fadeInRight',
          //     'fadeInDown'          => 'fadeInDown',
          //     'flip'                => 'flip',
          //     'flipInX'             => 'flipInX',
          //     'flipInY'             => 'flipInY',
          //     'flash'               => 'flash',
          //     'pulse'               => 'pulse',
          //     'rollIn'              => 'rollIn',
          //     'rotateIn'            => 'rotateIn',
          //     'rotateInDownLeft'    => 'rotateInDownLeft',
          //     'rotateInDownRight'   => 'rotateInDownRight',
          //     'rollIn'              => 'rollIn',
          //     'shake'               => 'shake',
          //     'swing'               => 'swing',
          //     'wobble'              => 'wobble',
          //     'zoomIn'              => 'zoomIn',
              
          //   ),
          //   'multiple'        => false,
          //   'placeholder'     => 'Escolha uma animação',
          //   'select_all_none' => true,
          // ),
        )
      ),
    )
  );

  // TESES
  $meta_boxes[] = array(
    'id'        => 'tese',
    'title'     => 'Teses',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'               => 'tese_image',
        'name'             => 'Imagem',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 

      array(
        'id'   => 'tese_title',
        'name' => 'Título',
        'type' => 'text',
      ), 

      array(
        'id'   => 'tese_desc',
        'name' => 'Descrição',
        'type' => 'textarea',
      ), 

      array(
        'type' => 'heading',
        'name' => 'Área de interesse',
      ),

      array(
        'id'   => 'area_text',
        'name' => 'Título',
        'type' => 'text',
      ), 

      array(
        'id'          => 'teses_interesse',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '8',
        'group_title' => 'Área de interesse',
        'save_state' => true,
        'fields' => array(

          array(
            'id'   => 'interesse_item',
            'name' => '',
            'type' => 'wysiwyg',
          ),  
          
        )
      ),

      array(
        'type' => 'heading',
        'name' => 'Área de interesse Tecnologia',
      ),

      array(
        'id'   => 'area_tec_text',
        'name' => 'Título',
        'type' => 'text',
      ), 
      
      array(
        'id'          => 'teses_tecnologia',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '8',
        'group_title' => 'Área de interesse',
        'save_state' => true,
        'fields' => array(

          array(
            'id'   => 'teses_tecnologia_item',
            'name' => '',
            'type' => 'text',
          ),  
          
        )
      ),
      
    )
  );

  // INVESTIMENTO
  $meta_boxes[] = array(
    'id'        => 'investimento',
    'title'     => 'Investimento',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'   => 'inv_title',
        'name' => 'Título',
        'type' => 'text',
      ), 

      array(
        'id'   => 'inv_text',
        'name' => 'Descrição',
        'type' => 'wysiwyg',
      ), 
    )
  );
 
  // MENTORES
  $meta_boxes[] = array(
    'id'        => 'mentores',
    'title'     => 'Mentores',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'   => 'mentores_title',
        'name' => 'Título',
        'type' => 'text',
      ),

      array(
        'type' => 'heading',
        'name' => 'Mentores',
      ),

      array(
        'id'          => 'mentores_carrousel',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '20',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'mentores_owl_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 

          array(
            'id'   => 'mentores_owl_title',
            'name' => 'Título',
            'type' => 'text',
          ), 
    
          array(
            'id'   => 'mentores_owl_desc',
            'name' => 'Descrição',
            'type' => 'textarea',
          ),
        )
      ),
    )
  );


  // CRONOGRAMA
  $meta_boxes[] = array(
    'id'        => 'cronograma',
    'title'     => 'Cronograma',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'   => 'cronograma_title',
        'name' => 'Título',
        'type' => 'text',
      ),

      array(
        'type' => 'heading',
        'name' => 'Timeline',
      ),

      array(
        'id'          => 'cronograma_options',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '5',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'cronograma_options_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 

          array(
            'id'               => 'cronograma_options_data',
            'name'             => 'Data',
            'type'             => 'text',
          ),
          
          array(
            'id'   => 'cronograma_options_title',
            'name' => 'Título',
            'type' => 'text',
          ), 
    
          array(
            'id'   => 'cronograma_options_desc',
            'name' => 'Descrição',
            'type' => 'wysiwyg',
          ),
        )
      ),
    )
  );

  // BENEFICIOS
  $meta_boxes[] = array(
    'id'        => 'beneficios',
    'title'     => 'Benefícios',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(
      
      array(
        'id'               => 'beneficios_img',
        'name'             => 'Imagem',
        'type'             => 'image_advanced',
        'max_file_uploads' => 1,
      ), 

      array(
        'id'   => 'beneficios_title',
        'name' => 'Título',
        'type' => 'text',
      ),

      array(
        'type' => 'heading',
        'name' => 'Benefícios',
      ),


      array(
        'id'          => 'beneficios_options',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '10',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'beneficios_options_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 
          
          array(
            'id'   => 'beneficios_options_title',
            'name' => 'Título',
            'type' => 'text',
          ), 

          array(
            'id'   => 'beneficios_options_desc',
            'name' => 'Descrição',
            'type' => 'textarea',
          ),
        )
      ),

    ),
  );

  // FEEDBACK
  $meta_boxes[] = array(
    'id'        => 'feedback',
    'title'     => 'Feedback',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array( 

      array(
        'id'   => 'feedback_title',
        'name' => 'Título',
        'type' => 'text',
      ),

      array(
        'id'   => 'feedback_desc',
        'name' => 'Descrição',
        'type' => 'textarea',
      ),

      array(
        'type' => 'heading',
        'name' => 'Feedbacks',
      ),

      array(
        'id'          => 'feedback_options',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '8',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'feedback_options_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 
          
          array(
            'id'   => 'feedback_cor',
            'name' => 'Cor do título',
            'type' => 'color',
          ),

          array(
            'id'   => 'feedback_options_title',
            'name' => 'Título',
            'type' => 'text',
          ), 

          array(
            'id'   => 'feedback_options_desc',
            'name' => 'Descrição',
            'type' => 'textarea',
          ),
        )
      ),

    ),
  );

  // FOOTER
   $meta_boxes[] = array(
    'id'        => 'footer',
    'title'     => 'Footer',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array( 

      array(
        'type' => 'heading',
        'name' => 'Mantenedores',
      ),

      array(
        'id'          => 'footer_mantenedores',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '4',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'footer_mantenedores_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 
         
        )
      ),

      array(
        'type' => 'heading',
        'name' => 'Parceiros',
      ),

      array(
        'id'          => 'footer_parceiros',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '18',
        'group_title' => 'Item',
        'save_state' => true,
        'fields' => array(

          array(
            'id'               => 'footer_parceiros_img',
            'name'             => 'Imagem',
            'type'             => 'image_advanced',
            'max_file_uploads' => 1,
          ), 
         
        )
      ),

      array(
        'type' => 'heading',
        'name' => 'Mídias Sociais',
      ),

      array(
        'id'   => 'footer_youtube',
        'name' => 'URL Youtube',
        'type' => 'url',
      ),

      array(
        'id'   => 'footer_instagram',
        'name' => 'URL Instagram',
        'type' => 'url',
      ),

      array(
        'id'   => 'footer_mail',
        'name' => 'URL Email',
        'type' => 'text',
      ),

      array(
        'id'   => 'footer_linkedin',
        'name' => 'URL Linkedin',
        'type' => 'url',
      ),

    ),
  );


  $meta_boxes[] = array(
    'id'        => 'section_startups',
    'title'     => 'Startups',
    'context'   => 'normal',
    'priority'  => 'high',
    'settings_pages' => 'theme-options',
    'fields'    => array(

      array(
        'id'    => 'section-startups-title',
        'name'  => 'Insira o titulo da Sessão',
        'type'  => 'textarea',
      ),

      array(

        'id'          => 'section-startups-cards',
        'name'        => '',
        'type'        => 'group',
        'clone'       => true,
        'sort_clone'  => true,
        'collapsible' => true,
        'max_clone'       => '15',
        'group_title' => 'cards',
        'save_state' => true,

          'fields' => array( 
            array(
              'id'   => 'section-startups-img',
              'name'  => 'cadastrar imagem',
              'type' => 'file_advanced',
              'max_file_uploads' => 1
            ),
    
            array(
              'id'    => 'section-startups-link',
              'name'  => 'insira o link da logo',
              'type'  => 'text',
            ),
          )
        ),
      
      ),
    );

  return $meta_boxes;
}


 