<?php
/*
* AJAX functions
*/

function getcidadesAjax() {

    global $wpdb;
    $tabela  = $wpdb->prefix."ajaxcidade";
    $estados = $_POST['estado'];

    if($estados == '') : echo '<option value="">Cidade</option>'; else :
        $myrows  = $wpdb->get_results( "SELECT * FROM $tabela WHERE estados_cod_estados=$estados GROUP BY nome ORDER BY nome");

        // print_r($myrows);

        echo '<option value="" selected="selected">Cidade</option>';
        foreach ($myrows as $row) {
            echo '<option value="'.$row->nome.'">'.$row->nome.'</option>';
        }
    endif;
    exit;
}

//add_action('wp_ajax_getcidades', 'getcidadesAjax');
//add_action('wp_ajax_nopriv_getcidades', 'getcidadesAjax');
