<?php
/*
* Share Functions
* Desenvolvedor: Bruno Kiedis
*/

//=========================================================================================
// COMPARTILHAR
//=========================================================================================

function get_compartilhe($link, $url_insta, $cobertura_link_foto, $cobertura_title) {

    $share = "javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;";
    
    
    $compartilhe  = '<ul class="share share--small">';
    
    if($cobertura_link_foto != null) { 
        // $textpre = $cobertura_title .' ☛ ' . $link . $cobertura_foto_true . ' Veja essa foto ☛ ' . $cobertura_link_foto; 
        $textpre = ' Veja essa foto ☛ ' . $cobertura_link_foto . ' Por BaladaIN ☛ http://www.baladain.com.br' ; 
        
    }
    else { $textpre = $link; }
    
    if ( wp_is_mobile() ) {
    $compartilhe .= '<li class="share__item">
        <a class="share__link" href="https://api.whatsapp.com/send?text='.$textpre.'">
            <i class="share__icone fa fa-whatsapp"></i>
        </a>
    </li>';
    } else {
      $compartilhe .= '<li class="share__item">
        <a class="share__link" href="https://web.whatsapp.com/send?text='.$textpre.'">
            <i class="share__icone fa fa-whatsapp"></i>
        </a>
    </li>';  
        
    }
    
    
    $compartilhe .= '<li class="share__item">
        <a class="share__link" href="https://www.facebook.com/sharer/sharer.php?u='.$link.'" onclick="'.$share.'">
            <i class="share__icone fa fa-facebook"></i><span>Facebook</span>
        </a>
    </li>';
    $compartilhe .= '<li class="share__item">
        <a class="share__link" href="https://www.linkedin.com/shareArticle?mini=true&url='.$link.'" onclick="'.$share.'">
            <i class="share__icone fa fa-linkedin"></i><span>Instagram</span>
        </a>
    </li>';

    if($url_insta):
    $compartilhe .= '<li class="share__item">
        <a class="share__link" href="'.$url_insta.'" target="_blank">
            <i class="share__icone fa fa-instagram"></i><span>Instagram</span>
        </a>
    </li>';
    endif;
    $compartilhe .= '<li class="share__item">
        <a class="share__link" href="http://twitter.com/share?url='.$link.'" onclick="'.$share.'">
            <i class="share__icone fa fa-twitter"></i><span>Twitter</span>
        </a>
    </li>';
    $compartilhe .= '</ul>';
    echo $compartilhe;
}

function wp_midia_social($atts) {
    extract(shortcode_atts(array(
    'facebook'    => '',
    'twitter'     => '',
    'gplus'       => '',
    'instagram'   => '',
    'youtube'     => '',
    'foursquare'  => '',
    'rss'         => '',
    'site'        => '',
    'email'       => '',
    ), $atts));
    //$social = '<ul>';
    if($facebook) {$social .= '<li><a class="icon-facebook" href="https://www.facebook.com/'.$facebook.'" target="_blank" title="Curta nossa página" alt="Curta nossa página"></a></li>';}
    if($gplus) {$social .= '<li><a class="icon-gplus" href="https://instagram.com/'.$gplus.'" target="_blank" title="Siga Círculo" alt="Siga Círculo"></a></li>';}
    if($twitter) {$social .= '<li><a class="icon-twitter" href="https://twitter.com/'.$twitter.'" target="_blank" title="Siga no Twitter" alt="Siga no Twitter"></a></li>';}
    if($instagram) {$social .= '<li><a class="icon-instagram" href="http://instagram.com/'.$instagram.'" target="_blank" title="Siga no Instagram" alt="Siga no Instagram"></a></li>';}
    if($youtube) {$social .= '<li><a class="icon-youtube" href="http://www.youtube.com/user/'.$youtube.'" target="_blank" title="Acesse nosso Canal" alt="Acesse nosso Canal"></a></li>';}
    if($foursquare) {$social .= '<li><a class="icon-foursquare" href="https://pt.foursquare.com/'.$foursquare.'" target="_blank" title="Acesse no Foursquare" alt="Acesse no Foursquare"></a></li>';}
    if($rss) {$social .= '<li><a class="icon-rss" href="'.$rss.'" target="_blank" title="Assine nosso RSS" alt="Assine nosso RSS"></a></li>';}
    if($site) {$social .= '<li><a class="icon-site" href="'.$site.'" target="_blank" title="Acesse nosso site" alt="Acesse nosso site"></a></li>';}
    if($email) {$social .= '<li><a class="icon-email" href="mailto:'.$email.'" title="Enviar e-mail" alt="Enviar e-mail"></a></li>';}
    //$social .= '</ul>';
    echo $social;
}

add_shortcode('midias_sociais', 'wp_midia_social');