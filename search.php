<?php get_template_part('templates/html','header'); ?>
<section class="pa-section-blog-banner">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" class="thumb">
    <div class="container">
        <div class="pa-section-blog-banner__title">
            <h2 class="wow fadeInUp">Resultado</h2>
        </div>
    </div>
</section>
<div class="pa-single">
    <div class="container">
        <div class="pa-single-content">
            <?php global $exclud_id_postagem;
                if ( have_posts() ) : ?>    
            
            <div class="pa-component-wrap-postagem">
            
                <?php while (have_posts()) : the_post(); 
                    $exclud_id_postagem[] = get_the_ID(); ?>

                    <?php include(locate_template('templates/content/loop-post.php')); ?>
                    
                <?php endwhile; ?>

            </div>
            <?php else : ?>

                <?php include(locate_template('templates/content/404.php')); ?>

            <?php endif; ?>
            
        </div>
    </div>
</div>
<?php include(locate_template('templates/outras.php')); ?>
<?php get_template_part('templates/html','footer');?>
