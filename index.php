<?php get_template_part('templates/html', 'header'); ?>
<div class="pa-page">
    <section class="pa-section-blog-banner">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner.jpg" alt="" class="thumb">
        <div class="container">
            <div class="pa-section-blog-banner__title">
                <h2 class="wow fadeInUp">Blog</h2>
            </div>
        </div>
    </section>
    <section class="pa-section-postagem">
        <div class="container">
            <div class="pa-component-wrap-postagem">
                <?php if ( have_posts() ) : ?>
                    <?php $count = 0; while ( have_posts() ) : the_post(); ?>
        
                        <?php include(locate_template('templates/content/loop-post.php')); ?>
                       
                        <?php if($count == 5): ?>
                            <div class="pa-component-wrap-newsletter">
                                <header class="pa-component-wrap-newsletter__header">
                                    <h2 class="title">Fique por dentro antes de todo mundo</h2>
                                    <p class="desc">Olá, Praieiro! Quer ficar por dentro de todas as novidades do Praia? Então assine a brisa, a newsletter onde os bons ventos sempre sopram.</p>
                                </header>
                                <div class="pa-component-wrap-newsletter__form">
                                    <?php echo do_shortcode('[contact-form-7 id="94" title="Formulário Newsletter"]'); ?>
                                </div>
                            </div>  
                        <?php endif; ?>

                    <?php $count++; endwhile; wp_pagenav($wp_query->max_num_pages); wp_reset_postdata(); else : ?>
                        <p><?php __('No News'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('templates/html', 'footer'); ?>