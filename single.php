<?php get_template_part('templates/html','header'); ?>
<div class="pa-single">
    <div class="container">
        <div class="pa-single-content">
            <div class="pa-single-content__post">
                <?php 
                    global $exclud_id_postagem;
                    while (have_posts()) : the_post(); 
                    $exclud_id_postagem[] = get_the_ID();
                ?>

                <?php include(locate_template('templates/content/content.php')); ?>
                
                <?php endwhile; ?>
            </div>
            <?php include(locate_template('sidebar.php')); ?>
        </div>
    </div>
</div>
<?php include(locate_template('templates/outras.php')); ?>
<?php get_template_part('templates/html','footer');?>
