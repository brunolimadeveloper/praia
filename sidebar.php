<?php $settings = get_option('options_gerais'); ?>
<aside class="pa-component-aside">
    <div class="pa-component-aside-recentes">
        <header class="pa-component-aside__header">  
            <h3 class="pa-component-aside__title">Recentes</h3>
            <hr class="line">
        </header> 
        <?php $recents = new WP_Query( array('post_type'=>'post','posts_per_page'=>3,"orderby"=>"date",'post__not_in' =>$exclud_id_postagem,"order"=> "DESC" )); 
            if ( $recents->have_posts() ) : 
                while ( $recents->have_posts() ) : $recents->the_post(); 
        ?>
            <article class="pa-component-aside-recentes-article">
                <figure class="pa-component-aside-recentes-article__figure">
                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                        <?php the_post_thumbnail('thumb-medium', array('class' => 'fade thumb', 'alt' => get_the_title(), 'title' => get_the_title())); ?>
                    </a>
                </figure>
                <div class="pa-component-aside-recentes-article__legend">
                    <a href="<?php echo get_category_link($cat[0]->term_id); ?>" class="pa-component-aside-recentes-article__categoria" style="color: <?php echo $color ? $color : '#4c13fb'; ?>;"><i class="pa-icon pa-icon--folder"></i><span><?php echo $cat[0]->name; ?></span></a>
                    <a href="<?php the_permalink(); ?>"><h2 class="pa-component-aside-recentes-article__title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                    <a href="<?php the_permalink(); ?>" class="pa-component-aside-recentes-article__link" title="<?php the_title_attribute('before=Ver mais sobre '); ?>">Ver mais <i class="fa fa-angle-right"></i></a>
                </div>
            </article>
            
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <p><?php __('No News'); ?></p>
        <?php endif; ?>
                   
    </div>
    <?php $tags = wp_get_post_terms(get_the_ID(), 'post_tag');
        if( $tags ): ?>
            <div class="pa-component-aside-tags">
                <header class="pa-component-aside__header">  
                    <h3 class="pa-component-aside__title">Tags</h3>
                    <hr class="line">
                </header> 
                <div class="pa-component-aside-tags__bloco">
                    <?php foreach ( $tags as $tag ) :
                        $tag_link = get_tag_link( $tag->term_id ); ?> 
                        <a href="<?php echo $tag_link; ?>" title="<?php echo $tag->name;?>" class="tag"><?php echo $tag->name;?></a>
                    <?php endforeach; ?>
                </div>
            </div>
    <?php endif; ?>
    <div class="pa-component-aside-network">
        <header class="pa-component-aside__header">  
            <h3 class="pa-component-aside__title">Compartilhe</h3>
            <hr class="line">
        </header> 
        <div class="pa-component-aside-network__midias">
            <a href="<?php echo $settings['footer_facebook'];?>" title="Facebook" class="link"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
            <a href="<?php echo $settings['footer_instagram'];?>" title="Instagram" class="link"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="<?php echo $settings['footer_twitter'];?>" title="Twitter" class="link"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="<?php echo $settings['footer_linkedin'];?>" title="Linkedin" class="link"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="pa-component-aside-newsletter">
        <header class="pa-component-aside__header">  
            <h3 class="pa-component-aside__title">Newsletter</h3>
            <hr class="line">
        </header> 
        <div class="pa-component-aside-newsletter__form">
            <?php echo do_shortcode('[contact-form-7 id="94" title="Formulário Newsletter"]'); ?>           
        </div>
    </div>
</aside>
