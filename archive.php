<?php get_template_part('templates/html', 'header'); ?>
<div class="pa-page">
    <section class="pa-section">
        <div class="container">
            <div class="pa-component-wrap-postagem">
                <?php include(locate_template('templates/content/loop-post.php')); ?>
            </div>
        </div>
    </section>
</div>
<?php get_template_part('templates/html', 'footer'); ?>